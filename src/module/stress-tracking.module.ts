import { Module } from '@nestjs/common'
import { StressTrackingController } from '../controller/stress-tracking/stress-tracking.controller'
import { stressTrackingRepositoryProviders, stressTrackingServiceProviders } from '../provider/stress-tracking.provider'

@Module({
    controllers: [
        StressTrackingController,
    ],
    providers: [
        stressTrackingServiceProviders,
        ...stressTrackingRepositoryProviders,
    ],
    imports: [],
    exports: [
        stressTrackingServiceProviders,
        ...stressTrackingRepositoryProviders,
    ],
})
export class StressTrackingModule {
}