import { Module } from '@nestjs/common'
import { ConfigModule } from '@nestjs/config'
import { CommonModule } from './common.module'
import { UserManagementModule } from './user-management.module'
import { LoginModule } from './login.module'
import { StressTrackingModule } from './stress-tracking.module'

@Module({
    imports: [
        ConfigModule.forRoot(),
        CommonModule,
        UserManagementModule,
        LoginModule,
        StressTrackingModule,
    ]
})

export class MainModule {}
