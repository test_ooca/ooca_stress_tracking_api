import { Module } from '@nestjs/common'
import { UserManagementController } from '../controller/user-management/user-management.controller'
import { userManagementRepositoryProviders, userManagementServiceProviders } from '../provider/user-management.provider'

@Module({
    controllers: [
        UserManagementController,
    ],
    providers: [
        userManagementServiceProviders,
        ...userManagementRepositoryProviders,
    ],
    imports: [],
    exports: [
        userManagementServiceProviders,
        ...userManagementRepositoryProviders,
    ],
})
export class UserManagementModule {
}