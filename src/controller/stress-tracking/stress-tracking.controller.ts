import { ApiBearerAuth, ApiConsumes, ApiOperation, ApiTags } from '@nestjs/swagger'
import {
    BadRequestException,
    Body,
    Controller,
    Get,
    Inject,
    Param,
    Post, Req,
    UploadedFiles,
    UseGuards,
    UseInterceptors,
} from '@nestjs/common'
import { providerNames } from '../../provider/provider.name'
import { StressTestValidator } from './stress-tracking.validator'
import { JwtAuthGuard } from '../../guards/jwt.guard'
import { FileFieldsInterceptor } from '@nestjs/platform-express'
import * as _ from 'lodash'
import { IStressTrackingService } from '../../domain/stress-tracking/interface/service.interface'
import { PassTokenGuard } from '../../guards/passToken.guard'
import { IUserPayloadJWT } from '../../domain/login/interface/service.interface'

@ApiTags('Stress-Tracking')
@Controller('/stress-tracking')
export class StressTrackingController {
    constructor(
        @Inject(providerNames.STRESS_TRACKING_SERVICE)
        private readonly _stressTrackingService: IStressTrackingService,
    ) {
    }

    @UseGuards(JwtAuthGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'get by id',
    })
    @Get('/:id')
    public getById(
        @Param('id') id: string,
    ) {
        return this._stressTrackingService.getById(id)
    }

    @UseGuards(PassTokenGuard)
    @ApiBearerAuth('jwt-token')
    @ApiOperation({
        summary: 'send stress test',
    })
    @Post('/')
    @UseInterceptors(
        FileFieldsInterceptor([
            {name: 'file', maxCount: 1},
        ], {
            fileFilter(req: any, file: { fieldname: string; originalname: string; encoding: string; mimetype: string; size: number; destination: string; filename: string; path: string; buffer: Buffer }, callback: (error: (Error | null), acceptFile: boolean) => void) {
                const type = ['image/jpg', 'image/jpeg', 'image/png']
                if (!type.some(value => {
                    return _.includes(file.mimetype, value)
                })) {
                    return callback(new BadRequestException('Only jpeg or png files are allowed!'), false)
                }
                callback(null, true)
            },
        }))
    @ApiConsumes('multipart/form-data')
    public stressTest(
        @Req() request: any,
        @Body() body: StressTestValidator,
        @UploadedFiles() files: Express.Multer.File[],
    ) {
        const userPayloadJWT: IUserPayloadJWT = {
            id: request.user['id'] || null,
            username: request.user['username'] || null,
            sessionId: request.user['sessionId'] || null,
        }
        return this._stressTrackingService.saveStress(body, files, userPayloadJWT)
    }
}