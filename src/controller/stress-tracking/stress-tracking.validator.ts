import { ApiProperty } from '@nestjs/swagger'
import { IsNotEmpty, IsNumber, IsOptional, Max, Min } from 'class-validator'
import { Transform } from 'class-transformer'
import { IStressTestValidator } from '../../domain/stress-tracking/interface/validator.interface'

export class StressTestValidator implements IStressTestValidator {
    @ApiProperty({
        name: 'score',
        required: true,
        type: Number,
    })
    @Transform(({value}) => Number(value))
    @IsNotEmpty()
    @IsNumber()
    @Min(1)
    @Max(5)
    private score: number

    public getScore(): number {
        return this.score
    }

    @ApiProperty({
        type:'string',
        format:'binary',
        required: true,
    })
    @IsOptional()
    private file: any;

    public getImage(): any {
        return this.file
    }
}