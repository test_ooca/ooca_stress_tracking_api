import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { IUserManagementRepository } from '../domain/user-management/interface/repository.interface'
import { UserManagementService } from '../domain/user-management/user-management.service'
import { IUserManagementModel } from '../domain/user-management/interface/model.interface'
import { IUserManagementSchema } from '../repository/user-management/user-management.schema'
import { UserManagementRepository } from '../repository/user-management/user-management.repository'
import { UserManagementRepositoryMapping } from '../repository/user-management/user-management.mapping'

export const userManagementRepositoryProviders: Provider[] = [
    {
        provide: providerNames.USER_MANAGEMENT_REPOSITORY_MAPPING,
        useClass: UserManagementRepositoryMapping,
    },
    {
        provide: providerNames.USER_MANAGEMENT_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.USER_MANAGEMENT_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IUserManagementModel, IUserManagementSchema>,
        ) => {
            return new UserManagementRepository(db, mapping)
        },
    },
]

export const userManagementServiceProviders: Provider = {
    provide: providerNames.USER_MANAGEMENT_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.USER_MANAGEMENT_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        usersRepository: IUserManagementRepository,
    ) => {
        return new UserManagementService(
            usersRepository,
        )
    },
}