import { Provider } from '@nestjs/common'
import { providerNames } from './provider.name'
import { Db } from 'mongodb'
import { IRepositoryMapping } from '../common/interface/repository.interface'
import { IConfig } from '../common/interface/config.interface'
import { StressTrackingRepositoryMapping } from '../repository/stress-tracking/stress-tracking.mapping'
import { IStressTrackingModel } from '../domain/stress-tracking/interface/model.interface'
import { StressTrackingService } from '../domain/stress-tracking/stress-tracking.service'
import { IStressTrackingRepository } from '../domain/stress-tracking/interface/repository.interface'
import { StressTrackingRepository } from '../repository/stress-tracking/stress-tracking.repository'
import { IStressTrackingSchema } from '../repository/stress-tracking/stress-tracking.schema'

export const stressTrackingRepositoryProviders: Provider[] = [
    {
        provide: providerNames.STRESS_TRACKING_REPOSITORY_MAPPING,
        useClass: StressTrackingRepositoryMapping,
    },
    {
        provide: providerNames.STRESS_TRACKING_REPOSITORY,
        inject: [
            providerNames.MONGO_CONNECTION,
            providerNames.STRESS_TRACKING_REPOSITORY_MAPPING,
        ],
        useFactory: (
            db: Db,
            mapping: IRepositoryMapping<IStressTrackingModel, IStressTrackingSchema>,
        ) => {
            return new StressTrackingRepository(db, mapping)
        },
    },
]

export const stressTrackingServiceProviders: Provider = {
    provide: providerNames.STRESS_TRACKING_SERVICE,
    inject: [
        providerNames.CONFIG,
        providerNames.STRESS_TRACKING_REPOSITORY,
    ],
    useFactory: (
        config: IConfig,
        stressTrackingRepository: IStressTrackingRepository,
    ) => {
        return new StressTrackingService(
            stressTrackingRepository,
        )
    },
}