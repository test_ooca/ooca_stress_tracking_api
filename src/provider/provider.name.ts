enum CommonProviderNames {
    CONFIG = 'common-config-provider',
    MONGO_CONNECTION = 'mongo-connection-provider',
}

enum JWTServiceProviderNames {
    JWT_SERVICE = 'jwt-service-provider',
    JWT_STRATEGY = 'jwt-strategy-service-provider'
}

enum LoginServiceProviderNames {
    LOGIN_SERVICE = 'login-service-provider'
}

enum UserManagementProviderNames {
    USER_MANAGEMENT_REPOSITORY = 'user-management-repository-provider',
    USER_MANAGEMENT_REPOSITORY_MAPPING = 'user-management-repository-mapping-provider',
    USER_MANAGEMENT_SERVICE = 'user-management-service-provider'
}

enum StressTrackingProviderNames {
    STRESS_TRACKING_REPOSITORY = 'stress-tracking-repository-provider',
    STRESS_TRACKING_REPOSITORY_MAPPING = 'stress-tracking-repository-mapping-provider',
    STRESS_TRACKING_SERVICE = 'stress-tracking-service-provider'
}

export const providerNames = Object.assign({},
    CommonProviderNames,
    JWTServiceProviderNames,
    UserManagementProviderNames,
    LoginServiceProviderNames,
    StressTrackingProviderNames,
)
