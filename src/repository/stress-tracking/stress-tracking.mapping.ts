import { plainToClass } from 'class-transformer'
import { ObjectId } from 'mongodb'
import * as _ from 'lodash'
import { IRepositoryMapping } from '../../common/interface/repository.interface'
import { IStressTrackingModel } from '../../domain/stress-tracking/interface/model.interface'
import { StressTrackingModel } from '../../domain/stress-tracking/stress-tracking.model'
import { IStressTrackingSchema } from './stress-tracking.schema'

export class StressTrackingRepositoryMapping implements IRepositoryMapping<IStressTrackingModel, IStressTrackingSchema> {
    public deserialize(schema: IStressTrackingSchema): IStressTrackingModel {
        return plainToClass(StressTrackingModel, {
            _id: schema._id.toHexString(),
            _userId: schema.userId,
            _score: schema.score,
            _image: schema.image,
            _createdAt: schema.createdAt,
        })
    }

    public serialize(model: IStressTrackingModel): IStressTrackingSchema {
        return {
            _id: !_.isNil(model.getId()) ? new ObjectId(model.getId()) : new ObjectId(),
            userId: model.getUserId(),
            score: model.getScore(),
            image: model.getImage(),
            createdAt: model.getCreatedAt(),
        }
    }

}