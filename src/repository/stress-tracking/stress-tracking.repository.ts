import { Db, ObjectId } from 'mongodb'
import { IStressTrackingSchema } from './stress-tracking.schema'
import { defaultIfEmpty, from, map, Observable } from 'rxjs'
import { BadRequestException, NotFoundException } from '@nestjs/common'
import * as _ from 'lodash'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'
import { IStressTrackingModel } from '../../domain/stress-tracking/interface/model.interface'
import { IStressTrackingRepository } from '../../domain/stress-tracking/interface/repository.interface'

export class StressTrackingRepository extends MongoRepository<IStressTrackingModel> implements IStressTrackingRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IStressTrackingModel, IStressTrackingSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.STRESS_TRACKING), mapping)
    }

    public getById(id: string): Observable<IStressTrackingModel> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException(`Invalid User ID`)
        }
        const promise = this.findOne({_id: new ObjectId(id)})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((usersModel: IStressTrackingModel) => {
                if (_.isNil(usersModel)) {
                    throw new NotFoundException(`User ID : ${id} Not Found`)
                }
                return usersModel
            }),
        )
    }
}