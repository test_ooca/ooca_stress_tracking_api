import { ObjectId } from 'bson'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export interface IStressTrackingSchema {
    _id: ObjectId
    userId: string
    score: number
    image: IAttachmentSchema
    createdAt: Date
}