import { ObjectId } from 'bson'

export interface IUserManagementSchema {
    _id: ObjectId
    username: string
    password: string
    name: string
    gender: GenderEnum
    address: string
    email: string
    telephone: string
    createdAt: Date
}

export enum GenderEnum {
    MALE = 'male',
    FEMALE = 'female',
    OTHER = 'other',
}