import { Db, ObjectId } from 'mongodb'
import { IUserManagementSchema } from './user-management.schema'
import { defaultIfEmpty, from, map, Observable } from 'rxjs'
import { BadRequestException, NotFoundException } from '@nestjs/common'
import * as _ from 'lodash'
import { IUserManagementModel } from '../../domain/user-management/interface/model.interface'
import { IUserManagementRepository } from '../../domain/user-management/interface/repository.interface'
import { IRepositoryMapping, MongoCollectionNameEnum } from '../../common/interface/repository.interface'
import { MongoRepository } from '../../common/mongo-repository'

export class UserManagementRepository extends MongoRepository<IUserManagementModel> implements IUserManagementRepository {

    constructor(
        db: Db,
        mapping: IRepositoryMapping<IUserManagementModel, IUserManagementSchema>,
    ) {
        super(db.collection(MongoCollectionNameEnum.USER_MANAGEMENT), mapping)
    }

    public getById(id: string): Observable<IUserManagementModel> {
        if (!ObjectId.isValid(id)) {
            throw new BadRequestException(`Invalid User ID`)
        }
        const promise = this.findOne({_id: new ObjectId(id)})
        return from(promise).pipe(
            defaultIfEmpty(null),
            map((usersModel: IUserManagementModel) => {
                if (_.isNil(usersModel)) {
                    throw new NotFoundException(`User ID : ${id} Not Found`)
                }
                return usersModel
            }),
        )
    }
}