import { IUserManagementService } from './interface/service.interface'
import { Observable } from 'rxjs'
import { IUserManagementRepository } from './interface/repository.interface'
import { UserManagementBuilder } from './user-management.builder'
import { IRegisterValidator } from './interface/validator.interface'
import { IUserManagementModel } from './interface/model.interface'

export class UserManagementService implements IUserManagementService {
    constructor(
        private readonly _usersRepository: IUserManagementRepository,
    ) {
    }

    public register(data: IRegisterValidator): Observable<string> {
        const newUser = new UserManagementBuilder()
        newUser.setUsername(data.getUsername())
        newUser.setPassword(data.getPassword())
        newUser.setName(data.getName())
        newUser.setGender(data.getGender())
        newUser.setAddress(data.getAddress())
        newUser.setEmail(data.getEmail())
        newUser.setTelephone(data.getTelephone())
        return this._usersRepository.insert(newUser.build())
    }

    public getById(id: string): Observable<IUserManagementModel> {
        return this._usersRepository.getById(id)
    }
}