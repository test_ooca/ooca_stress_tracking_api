import { ILoginService, IUserPayloadJWT } from './interface/service.interface'
import { NotFoundException } from '@nestjs/common'
import { LoginValidator } from '../../controller/login/login.validator'
import { defaultIfEmpty, mergeMap, Observable, of } from 'rxjs'
import { JwtService, JwtSignOptions } from '@nestjs/jwt'
import * as _ from 'lodash'
import * as uuid from 'uuid'
import { IncomingHttpHeaders } from 'http'
import { IUserManagementRepository } from '../user-management/interface/repository.interface'
import { IUserManagementModel } from '../user-management/interface/model.interface'

export class LoginService implements ILoginService {
    private readonly _jwtSignOption: JwtSignOptions

    constructor(
        private readonly _jwtService: JwtService,
        private readonly _usersRepository: IUserManagementRepository,
    ) {
        this._jwtSignOption = {
            expiresIn: '3h',
        }
    }

    public login(payload: LoginValidator, httpHeaders: IncomingHttpHeaders): Observable<string> {
        const findUserFilter = {
            username: payload.getUsername(),
            password: payload.getPassword(),
        }
        return this._usersRepository.findOne(findUserFilter).pipe(
            defaultIfEmpty(null),
            mergeMap((userModel: IUserManagementModel) => {
                if (_.isNil(userModel)) {
                    throw new NotFoundException(`Login Failed: invalid Username or Password`)
                }
                const sessionId = uuid.v4()
                const userPayloadJWT: IUserPayloadJWT = {
                    id: userModel.getId(),
                    username: payload.getUsername(),
                    sessionId,
                }
                return of(this._jwtService.sign(userPayloadJWT, this._jwtSignOption))
            }),
        )
    }

}