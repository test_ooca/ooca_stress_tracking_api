import { IEntity } from '../../../common/interface/entity.interface'
import { IAttachmentSchema } from '../../../common/interface/schema.interface'

export interface IStressTrackingModel extends IEntity {
    getUserId():string
    setUserId(userId: string): void

    getScore(): number
    setScore(score: number): void

    getImage(): IAttachmentSchema
    setImage(image: IAttachmentSchema)

}