import { Observable } from 'rxjs'
import { IStressTrackingModel } from './model.interface'
import { IStressTestValidator } from './validator.interface'
import { IUserPayloadJWT } from '../../login/interface/service.interface'

export interface IStressTrackingService {
    saveStress(data: IStressTestValidator, files: any, userPayload: IUserPayloadJWT): Observable<string>

    getById(id: string): Observable<IStressTrackingModel>
}

