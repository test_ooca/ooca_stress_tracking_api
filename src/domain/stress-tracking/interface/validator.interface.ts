export interface IStressTestValidator {
    getScore(): number

    getImage(): any
}