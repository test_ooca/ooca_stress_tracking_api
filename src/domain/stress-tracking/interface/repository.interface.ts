import { IStressTrackingModel } from './model.interface'
import { Observable } from 'rxjs'
import { IRepository } from '../../../common/interface/repository.interface'

export interface IStressTrackingRepository extends IRepository<IStressTrackingModel> {
    getById(id: string): Observable<IStressTrackingModel>
}