import { IStressTrackingService } from './interface/service.interface'
import { Observable } from 'rxjs'
import { IStressTrackingRepository } from './interface/repository.interface'
import { StressTrackingBuilder } from './stress-tracking.builder'
import { IStressTestValidator } from './interface/validator.interface'
import { IStressTrackingModel } from './interface/model.interface'
import * as _ from 'lodash'
import * as fs from 'fs'
import * as Path from 'path'
import * as moment from 'moment'
import * as uuid from 'uuid'
import { InternalServerErrorException } from '@nestjs/common'
import { IAttachmentSchema } from '../../common/interface/schema.interface'
import { IUserPayloadJWT } from '../login/interface/service.interface'

export class StressTrackingService implements IStressTrackingService {
    constructor(
        private readonly _stressTrackingRepository: IStressTrackingRepository,
    ) {
    }

    public saveStress(data: IStressTestValidator, files: Express.Multer.File[], userPayload: IUserPayloadJWT): Observable<string> {
        const file = _.get(files, 'file[0]', null)
        const fileSchema = this._saveFile(file)
        const userId = !_.isNil(userPayload.id) ? userPayload.id : 'anonymous'
        const newStress = new StressTrackingBuilder()
        newStress.setUserId(userId)
        newStress.setScore(data.getScore())
        newStress.setImage(fileSchema)
        return this._stressTrackingRepository.insert(newStress.build())
    }

    public getById(id: string): Observable<IStressTrackingModel> {
        return this._stressTrackingRepository.getById(id)
    }

    private _saveFile(file: Express.Multer.File, compareFile?): IAttachmentSchema {
        if (_.isNil(file)) {
            return null
        }
        if (!_.isNil(compareFile)) {
            if (_.isEqual(file.originalname, compareFile.name) && _.isEqual(file.size, compareFile.size) && _.isEqual(file.mimetype, compareFile.contentType)) {
                return compareFile
            }
        }
        const folderName = moment().format('YYYYMMDD')
        let savePath = `./upload/stress/${folderName}`
        if (!fs.existsSync(savePath)) {
            fs.mkdirSync(savePath, {recursive: true})
        }
        let fileName = `${uuid.v4()}${Path.extname(file.originalname)}`
        savePath += `/${fileName}`
        try {
            fs.writeFileSync(savePath, file.buffer)
        } catch (err) {
            if (err) {
                throw new InternalServerErrorException(`Cannot write file ${file.originalname}`)
            }
        }
        return {
            data: {
                link: savePath,
            },
            alt: fileName,
            name: fileName,
            originalName: file.originalname,
            size: file.size,
            contentType: file.mimetype,
        }
    }
}