import { IStressTrackingModel } from './interface/model.interface'
import { StressTrackingModel } from './stress-tracking.model'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export class StressTrackingBuilder {
    private readonly _model: IStressTrackingModel

    constructor() {
        this._model = new StressTrackingModel()
    }

    public build(): IStressTrackingModel {
        return this._model
    }

    public setUserId(userId: string) {
        this._model.setUserId(userId)
        return this
    }

    public setScore(score: number) {
        this._model.setScore(score)
        return this
    }

    public setImage(image: IAttachmentSchema) {
        this._model.setImage(image)
        return this
    }
}