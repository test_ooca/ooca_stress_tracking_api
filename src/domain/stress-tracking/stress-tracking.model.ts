import { IStressTrackingModel } from './interface/model.interface'
import { Entity } from '../../common/entity'
import { IAttachmentSchema } from '../../common/interface/schema.interface'

export class StressTrackingModel extends Entity implements IStressTrackingModel {
    private _userId: string
    private _score: number
    private _image: IAttachmentSchema

    public getUserId(): string {
        return this._userId
    }

    public setUserId(userId: string): void {
        this._userId = userId
    }

    public getScore(): number {
        return this._score
    }

    public setScore(score: number): void {
        this._score = score
    }

    public getImage(): IAttachmentSchema {
        return this._image
    }

    public setImage(image: IAttachmentSchema) {
        this._image = image
    }
}